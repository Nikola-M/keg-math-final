/* const { option } = require('grunt'); */

export default function (grunt) {
  require('time-grunt')(grunt);
  require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin',
  });

  grunt.initConfig({
    sass: {
      dist: {
        files: [
          {
            expand: true,
            cwd: 'css',
            src: ['*.scss'],
            dest: 'css',
            ext: '.css',
          },
        ],
      },
    },

    watch: {
      files: ['app/resources/css/*.scss'],
      task: ['css'],
    },

    browserSync: {
      dev: {
        bsFiles: {
          //browser files
          src: ['css/*.css', '*.html', 'js/*.js'],
        },

        options: {
          wtchTask: true,
          server: {
            baseDir: './', // Directorio base para nuestro servidor
          },
        },
      },
    },

    imagemin: {
      dynamic: {
        files: [
          {
            expanded: true,
            cwd: './',
            src: 'assets/*.{png,gif,jpg,jpeg}',
            dest: 'dist/',
          },
        ],
      },
    },

    copy: {
      html: {
        files: [
          {
            expand: true,
            dot: true,
            cwd: './',
            src: ['*.html'],
            dest: 'dist',
          },
        ],
      },

      fonts: {
        files: [
          {
            expand: true,
            dot: true,
            cwd: 'node-modules/open-iconic/font',
            src: ['fonts/*.*'],
            dest: 'dist',
          },
        ],
      },
    },

    clean: {
      build: {
        src: ['dist/'],
      },
    },

    cssmin: {
      dist: {},
    },

    uglify: {
      dist: {},
    },

    filerev: {
      options: {
        encoding: 'utf8',
        algorithm: 'md5',
        length: 20,
      },

      release: {
        // filerev:release hashes(md5) all assets (imagenes, js & css)
        // in dist directory
        files: [
          {
            src: ['dist/js/*.js', 'dist/css/*.css'],
          },
        ],
      },
    },

    concat: {
      options: {
        separator: ';',
      },
      dist: {},
    },

    useminPrepare: {
      foo: {
        dest: 'dist',
        src: [
          'index.html',
          'nosotros.html',
          'contacto.html',
          'tutorias.html',
          'terminos.html',
        ],
      },

      options: {
        flow: {
          steps: {
            css: ['cssmin'],
            js: ['uglify'],
          },

          post: {
            css: [
              {
                name: 'cssmin',
                createConfig: function (context, block) {
                  var generated = context.options.generated;
                  generated.options = {
                    keepSpecialComments: 0,
                    rebase: false,
                  };
                },
              },
            ],
          },
        },
      },
    },

    usemin: {
      html: [
        'dist/index.html',
        'dist/contacto.html',
        'dist/terminos.html',
        'dist/tutorias.html',
        'dist/nosotros.html',
      ],
      options: {
        assetsDir: ['dist', 'dist/css', 'dist/js'],
      },
    },
  });

  grunt.registerTask('css', ['sass']);
  grunt.registerTask('default', ['browserSync', 'watch']);
  grunt.registerTask('img:compress', ['imagemin']);
  grunt.registerTask('build', [
    'clean',
    'copy',
    'imagemin',
    'useminPrepare',
    'concat',
    'cssmin',
    'uglify',
    'filerev',
    'usemin',
  ]);
}
