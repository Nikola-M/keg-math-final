$(function () {
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();
  $('.carousel').carousel({
    interval: 2000,
  });

  $('#tutoria').on('show.bs.modal', function (e) {
    console.log('El modal se esta mostrando');

    $('#tutoriaBtn').removeClass('btn-outline-success');
    $('#tutoriaBtn').addClass('btn-primary');
    $('#tutoriaBtn').prop('disabled', true);
  });

  $('#tutoria').on('shown.bs.modal', function (e) {
    console.log('El modal se mostro');
  });

  $('#tutoria').on('hide.bs.modal', function (e) {
    console.log('El modal se oculta');
  });

  $('#tutoria').on('hidden.bs.modal', function (e) {
    console.log('El modal se oculto');
    $('#tutoriaBtn').prop('disabled', false);
  });
});
